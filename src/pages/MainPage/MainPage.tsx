import * as React from "react";
import { FC } from "react";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Button } from "@mui/material";
import { useQuery } from "react-query";
import { API } from "../../core/api/API";
import { UserType } from "../../models/models";

const Row = (props: { row: UserType }) => {
  const { row } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.name}
        </TableCell>
        <TableCell align="left">{row.email}</TableCell>
        <TableCell align="left">{row.age}</TableCell>
        {/*<TableCell align="left">*/}
        {/*  <Button variant="contained" color="success">*/}
        {/*    Success*/}
        {/*  </Button>*/}
        {/*</TableCell>*/}
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                Accounts
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Account Number</TableCell>
                    <TableCell>Balance</TableCell>
                    <TableCell>Currency</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.accounts.map(({ accountNumber, balance, currency }) => (
                    <TableRow key={accountNumber}>
                      <TableCell component="th" scope="row">
                        {accountNumber}
                      </TableCell>
                      <TableCell>{balance}</TableCell>
                      <TableCell>{currency}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

const MainPage: FC = () => {
  const { data: users } = useQuery(
    ["getAllUsers"],
    () => {
      return API.User.getAllUsers();
    },
    {}
  );

  console.log("users", users);

  if (!users || !users.length) {
    return <Typography variant={"h3"}>Loading...</Typography>;
  }

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>User name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Age</TableCell>
            {/*<TableCell />*/}
            {/*<TableCell />*/}
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((row) => (
            <Row key={row.name} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default MainPage;
