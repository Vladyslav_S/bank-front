import axios from "axios";
import { ApiServer } from "../config";

const host = axios.create({
  baseURL: ApiServer,
  headers: {
    "content-type": "application/json",
  },
});

export { host };
