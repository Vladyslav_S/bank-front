import React from "react";
import MainPage from "../../pages/MainPage/MainPage";
import { Container } from "@mui/material";

function App() {
  return (
    <Container>
      <MainPage />
    </Container>
  );
}

export default App;
