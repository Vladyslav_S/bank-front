export type UserType = {
  id: number;
  name: string;
  age: number;
  email: string;
  accounts: AccountType[];
};

export type AccountType = {
  id: number;
  accountNumber: string;
  balance: number;
  currency: CurrencyType;
};

export enum CurrencyType {
  USD,
  EUR,
  UAH,
  CHF,
  GBP,
}
