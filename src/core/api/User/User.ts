import { host } from "../../../helpers/axiosConfig";
import { UserType } from "../../../models/models";

export class User {
  public async getAllUsers(): Promise<UserType[]> {
    const resp = await host.get("/customers");
    return resp.data;
  }
}
