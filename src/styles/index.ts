import { createTheme, Theme } from "@mui/material";
import { blue } from "@mui/material/colors";

export const createMuiTheme = (): Theme => {
  return createTheme({
    components: {
      MuiCssBaseline: {
        styleOverrides: `
          html {
            height: 100%;
          },
          body {
            height: 100%;
            display: flex;
            margin: 0;
          },
          #root {
            flex: 1 1 100%;
            display: flex;
            flex-direction: column;
          }
        `,
      },
      MuiTypography: {
        styleOverrides: {

        },
      },
    },
    typography: {},
    palette: {
      primary: {
        main: "#000",
      },
      secondary: {
        main: blue[500],
      },
    },
  });
};

export const theme = createMuiTheme();
